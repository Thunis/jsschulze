This implements the [Schulze election method](https://en.wikipedia.org/wiki/Schulze_method) with optional support for rejection of options.
The implementation is a single file of HTML with JavaScript.
It runs in any (JS-capable) browser *without a server* of any kind.
The code is open source, simple and documented with inline comments.

## Vote Format

The votes must be entered in this format: Each line is one vote.
Each vote must rank all options.
The options are named `A` to the uppercase letter corresponding to the number of options.
Consequently the number of options is at max 26.
Please fork and create a pull request, if this is a problem for you.
A vote is a sequence of options separated by comparators.
Valid comparators are `>`, `=` and `|`.
* `>` means the options to the left are prefered over the options to the right.
* `=` means the options directly adjacent are equally preferable.
* `|` is the same as `>`, but also means all candidates to the right should be rejected.
It can occur at most once per vote.

If you need an election without rejection, just don't input any rejection comparators (i.e. `|`).

### Example

    B>C=D|A>E

This means, the most preferable option is option B.
Second best and still acceptable are C and D.
A and E should be rejected entirely, but E would be even worse than A.

## Output

The tool outputs a ranking of the options using `>` and `=`, like the votes.
The rejected options are however listed separately.

### Example

    B=D>C>E>A|A E

The winners are B and D with an equal score.
Second is C.
Third best is option E.
Fourth best is option A.
Options A and E are however rejected.

### Further Output

The tool also visualises some in-between steps by showing the tables of preference and strongest path strengths (as in the [Wikipedia article about the Schulze method at the time of writing](https://en.wikipedia.org/w/index.php?title=Schulze_method&oldid=746413667)).
Unfortunately the tables are flipped in comparison to the Wikipedia example.

Screenshot of the result from the Wikipedia example:

![Screenshot](https://thunis-uni.de/jsschulze_screenshot.png)
